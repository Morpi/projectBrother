$(window).on('load scroll', function() {
	// show header when scroll page
	var	w_scroll = $(window).scrollTop(),
	slider = $('.slider-image').height();
	if (slider <= (w_scroll + 250)) {
		$('.header').addClass('fixed');
	}
	else {
		$('.header').removeClass('fixed');
	}

	if (slider <= (w_scroll + 150)) {
		$('.header').addClass('offset');
	} else {
		$('.header').removeClass('offset');
	}

	if (slider <= w_scroll) {
		$('.header').addClass('scrolling');
	} else {
		$('.header').removeClass('scrolling');
	}
})

$(document).ready(function() {
	
})